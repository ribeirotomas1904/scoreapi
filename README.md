## Requirements:
- [Docker](https://docs.docker.com/get-docker/)
 - [Docker Compose](https://docs.docker.com/compose/install/)
 
 ## Endpoint examples:
 
### Create score
**method**: POST

**url**: /score

**body**: 
```json
{ "cpf": "17783183049" }
```
**reponse**: 
```json
{ "score": 359 }
```
<hr/>

### Show score
**method**: GET

**url**: /score/17783183049

**reponse**: 
```json
{ "score": 359, "created_at": "2021-07-19T01:43:20.34328" }
```

## How to run:

 1. Run all services:
```bash
docker-compose -p datarisk --profile prod up -d
```
2. Run migrations:
```bash
docker exec -t datarisk_score-api_1 bash -c "./Migrations"
```

## How to test:

- Endpoints:
Importing ``score-api.postman_collection.json`` in Postman (preferably).

- Database persistence:
	1. Create a score with a given cpf.
	2. Run ``docker rm -f datarisk_postgres_1 `` to remove the database running container.
	3. Run ``docker-compose -p datarisk --profile prod up -d`` to create the database container again.
	4. Make a request to the "show score" endpoint using the same cpf as above and see that it is still present in the database (because of the docker volume).