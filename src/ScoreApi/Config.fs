module Config

type Config = {
    connectionString : string;
    cpfSalt : string;
}