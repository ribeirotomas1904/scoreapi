namespace Scores

type Score = {
  cpf: string
  score: int
  created_at: System.DateTime
}

type PartialScore = {
  score: int
  created_at: System.DateTime
}

type OnlyScore = {
  score: int
}

type OnlyCpf = {
  cpf: string
}

module Validation =
  let validate v =
    let validators = [
      fun u -> if isNull u.cpf then Some ("cpf", "Cpf shouldn't be empty") else None
    ]

    validators
    |> List.fold (fun acc e ->
      match e v with
      | Some (k,v) -> Map.add k v acc
      | None -> acc
    ) Map.empty
