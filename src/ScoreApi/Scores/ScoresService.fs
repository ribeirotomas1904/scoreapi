namespace Scores

open BCrypt.Net

module Service =

    let hashCpf cpfSalt cpf =
        let hash = BCrypt.HashPassword(cpf, cpfSalt)
        hash.Replace(cpfSalt, "")