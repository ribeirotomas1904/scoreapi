namespace Scores

open System
open Config
open Database
open Saturn
open Npgsql
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive

module Database =

  let getByCpf cnf cpf : Task<Result<PartialScore option, exn>> =
    task {
      use connection = new NpgsqlConnection(cnf.connectionString)

      let cpfHash = Service.hashCpf cnf.cpfSalt cpf
      return! querySingle connection "SELECT score, created_at FROM Scores WHERE cpf=@cpf" (Some <| dict ["cpf" => cpfHash])
    }

  let upsertSql =
    [ "INSERT INTO Scores(cpf, score, created_at)";
      "VALUES (@cpf, @score, @created_at)";
      "ON CONFLICT (cpf) DO";
      "UPDATE SET score = @score, created_at = @created_at";
      "WHERE Scores.cpf = @cpf";
    ] |> String.concat "\n"

  let upsert cnf cpf : Task<Result<OnlyScore,exn>> =
    task {
      use connection = new NpgsqlConnection(cnf.connectionString)

      let cpfHash = Service.hashCpf cnf.cpfSalt cpf
      let random = new Random()

      let newScore =
        { cpf = cpfHash;
          score = random.Next(1, 1001);
          created_at = DateTime.UtcNow;
        }

      let! result = execute connection upsertSql newScore
      match result with
        | Ok _ ->
          return Ok { score = newScore.score }
        | Error e ->
          return Error e
    }
