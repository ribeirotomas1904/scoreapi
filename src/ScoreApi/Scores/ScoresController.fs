namespace Scores

open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.ContextInsensitive
open Config
open Saturn

module Controller =

  let showAction (ctx: HttpContext) (cpf : string) =
    task {
      let cnf = Controller.getConfig ctx
      let! result = Database.getByCpf cnf cpf
      match result with
      | Ok (Some result) ->
        return! Response.ok ctx result
      | Ok None ->
        return! Response.notFound ctx "Value not found"
      | Error ex ->
        return raise ex
    }

  let createAction (ctx: HttpContext) =
    task {
      let! input = Controller.getModel<OnlyCpf> ctx
      let validateResult = Validation.validate input
      if validateResult.IsEmpty then
        let cnf = Controller.getConfig ctx
        let! result = Database.upsert cnf input.cpf
        match result with
        | Ok newScore ->
          return! Response.ok ctx newScore
        | Error ex ->
          return raise ex
      else
        return! Response.badRequest ctx "Validation failed"
    }

  let resource = controller {
    show showAction
    create createAction
  }

