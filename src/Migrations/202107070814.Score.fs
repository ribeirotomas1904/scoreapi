namespace Migrations
open SimpleMigrations

[<Migration(202107070814L, "Create Scores")>]
type CreateScores() =
  inherit Migration()

  override __.Up() =
    base.Execute(@"CREATE TABLE Scores(
      cpf TEXT UNIQUE NOT NULL,
      score INT NOT NULL,
      created_at TIMESTAMP NOT NULL
    )")

  override __.Down() =
    base.Execute(@"DROP TABLE Scores")
